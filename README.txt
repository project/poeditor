POEditor
========

This module helps to automatize the translation process of your Drupal 
localization project through POEditor. POEditor is a crowd-sourcing translation
platform. It will connect your POEditor account to your Drupal admin interface
in order to easily sync the translations between the two.

Highlights of poeditor.com are:

- User-friendly translation environment
- Collaborative platform, optimized for crowdsourcing
- Project management oriented
- API and WordPress plugin for more automation
- Translation forecast and rich statistics
- Ideal for translating Android and iOS apps
- Unlimited for Open Source software
- Friendly and free support
